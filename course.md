# Markdown lernen

* Was ist Markdown?
* Wozu brauche ich das?


Markdown ist eine vereinfachte Auszeichnungssprache, die von John Gruber und Aaron Swartz entworfen und im Dezember 2004 mit Version 1.0.1 spezifiziert wurde. Ein Ziel von Markdown ist, dass schon die Ausgangsform ohne weitere Konvertierung leicht lesbar ist. 
[Wikipedia](https://de.wikipedia.org/wiki/Markdown)

![Logo Markdown](markdownbild.png)

![externes Bild einbinden](https://upload.wikimedia.org/wikipedia/commons/4/48/Markdown-mark.svg)

## Überschrift 2 - einen Editor verlinken
[Ein Markdown-Editor aus Gibhub](https://gitlab.com/denis-skripnik/simplemde-markdown-editor)
